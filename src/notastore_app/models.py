from django.db import models


class Store(models.Model):
    name = models.CharField(max_length=64)
    details = models.TextField(blank=True)
    logo = models.ImageField(upload_to='store/logos', blank=True, null=True)
    catalog = models.FileField(upload_to='store/files', blank=True, null=True)

    class Meta:
        ordering = ('name', )