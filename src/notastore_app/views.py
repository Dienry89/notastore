from django.views.generic import ListView


class StoreHomeView(ListView):
    model = Store
    queryset = Store.objects.all()
    template_name = 'store_home.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(*args, **kwargs)
        return {
            **ctx, 
            "store": Store.objects.get(pk=self.kwargs['pk']),
            "products": StoreProduct.objects.order_by("name").filter(store = self.kwargs['pk']),
        }
